package ru.peo;

import java.io.*;
import java.util.ArrayList;

public class Scan {
    public static void main(String[] args) throws IOException {
        String path = "C:\\Users\\User\\Desktop\\IDEApr\\txt";
        File dir = new File(path);
        ArrayList<File> buffer = new ArrayList<>();//пустой массив
        ArrayList<File> txtFiles = findTxtFiles(dir, buffer);//в этом массиве хранятся все найденные текстовые файлы

        for (File file : txtFiles) {
            try(BufferedReader reader = new BufferedReader(new FileReader(file.getAbsoluteFile()))) {
                String str;
                int count = 0;
                while ((str = reader.readLine()) != null) {
                    count++;
                    if(str.contains("Love")){
                        System.out.println("Путь к файлу: " + file.getName() + "\nНомер строки: " + count);
                    }
                }

            }catch(IOException ex){
                System.out.println("Фиг вам,ошибка");
            }
        }


    }

    public static ArrayList<File> findTxtFiles(File dir, ArrayList<File> txtFiles) {// текущая директория(File dir),
        // массив в который складываем найденые текстовые файлы(ArrayList<File> txtFiles)

        File[] files = dir.listFiles();//создаем массив
        for (File file : files) {
            if (file.isFile()) {
                if (file.getName().contains(".txt")) {
                    txtFiles.add(file);//**после проверки заполняет *** массив ткст файлами
                }
            }
            if (file.isDirectory()) {
                findTxtFiles(file, txtFiles);
            }
        }
        return txtFiles;
    }
}